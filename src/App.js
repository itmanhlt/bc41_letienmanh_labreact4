// import logo from "./logo.svg";
import "./App.css";
import ProductList from "./Phone_React/ProductList";

function App() {
  return (
    <div className="container">
      <ProductList />
    </div>
  );
}

export default App;
