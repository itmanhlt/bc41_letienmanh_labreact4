import React, { Component } from "react";

export default class ProductDetail extends Component {
  render() {
    let {
      tenSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      hinhAnh,
    } = this.props.detail;
    return (
      <div className="row">
        <div className="col-6">
          <h2 className="text-center text-success">{tenSP}</h2>
          <div style={{ height: "500px" }}>
            <img style={{ height: "100%" }} src={hinhAnh} alt="" />
          </div>
        </div>
        <div className="col-6">
          <h2 className="text-center text-success">Thông số kỹ thuật</h2>
          <div className="mt-5">
            <table>
              <tr>
                <td>Màn hình</td>
                <td>{manHinh}</td>
              </tr>
              <tr>
                <td>Hệ điều hành</td>
                <td>{heDieuHanh}</td>
              </tr>
              <tr>
                <td>Camera trước</td>
                <td>{cameraTruoc}</td>
              </tr>
              <tr>
                <td>Camerau Sau</td>
                <td>{cameraSau}</td>
              </tr>
              <tr>
                <td>Ram</td>
                <td>{ram}</td>
              </tr>
              <tr>
                <td>{rom}</td>
                <td>Rom</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
