import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    let { hinhAnh, tenSP, giaBan } = this.props.list;
    return (
      <div className="col-4">
        <div className="card shadow rounded" style={{ height: "100%" }}>
          <div className="mt-2" style={{ height: "100%" }}>
            {" "}
            <img src={hinhAnh} className="card-img-top" alt="..." />
          </div>
          <div className="card-body text-center">
            <h5 className="card-title">{tenSP}</h5>
            <h6>{giaBan.toLocaleString()} VNĐ</h6>
            <button
              onClick={() => this.props.handleDetail(this.props.list)}
              className="btn btn-success"
            >
              Xem chi tiết
            </button>
          </div>
        </div>
      </div>
    );
  }
}
