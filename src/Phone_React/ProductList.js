import React, { Component } from "react";
import { dataPhone } from "./dataPhone";
import ProductDetail from "./ProductDetail";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  state = {
    list: dataPhone,
    detail: dataPhone[0],
  };
  handleDetail = (phone) => {
    console.log(phone);
    this.setState({
      detail: phone,
    });
  };
  render() {
    return (
      <div>
        <h2 className="mt-1 text-success">Danh sách sản phẩm</h2>
        <div className="row">
          {this.state.list.map((item, index) => {
            return (
              <ProductItem
                handleDetail={this.handleDetail}
                list={item}
                key={index}
              />
            );
          })}
        </div>
        <div className="mt-5">
          <ProductDetail detail={this.state.detail} />
        </div>
      </div>
    );
  }
}
